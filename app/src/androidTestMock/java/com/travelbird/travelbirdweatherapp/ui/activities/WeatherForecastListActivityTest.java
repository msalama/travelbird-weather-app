package com.travelbird.travelbirdweatherapp.ui.activities;

import android.content.Intent;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.google.common.collect.Lists;
import com.travelbird.travelbirdweatherapp.FakeYahooWeatherForecastFetcher;
import com.travelbird.travelbirdweatherapp.R;
import com.travelbird.travelbirdweatherapp.data.model.Forecast;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.travelbird.travelbirdweatherapp.ui.activities.custom.matcher.CustomMatcher.withItemText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.Is.is;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class WeatherForecastListActivityTest {

    private static String CODE = "23";
    private static String DATE = "1/1/2016";
    private static String DAY = "Sun";
    private static String HIGH = "36";
    private static String LOW = "25";

    private static List<Forecast> forecastList =
            Lists.newArrayList(new Forecast(CODE, DATE, DAY, HIGH, LOW));

    @Rule
    public ActivityTestRule<WeatherForecastListActivity> activityActivityTestRule =
            new ActivityTestRule<>(WeatherForecastListActivity.class, true, false);

    @Test
    public void showForecastElementsToList() {
        FakeYahooWeatherForecastFetcher.setWeatherForecastList(forecastList);
        FakeYahooWeatherForecastFetcher.setIsSuccess(true);

        activityActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.forecast_list)).perform(scrollTo(hasDescendant(withText(DAY))));

        onView(withItemText(DATE)).check(matches(isDisplayed()));
        onView(withItemText(DAY)).check(matches(isDisplayed()));
        onView(withItemText(HIGH)).check(matches(isDisplayed()));
        onView(withItemText(LOW)).check(matches(isDisplayed()));

        onView(withId(R.id.loading_progressBar)).
                check(
                        matches(
                        withEffectiveVisibility(ViewMatchers.Visibility.GONE)
                        )
                );
    }

    @Test
    public void showSnackbarWhenFailedLoading() {
        FakeYahooWeatherForecastFetcher.setWeatherForecastList(forecastList);
        FakeYahooWeatherForecastFetcher.setIsSuccess(false);

        activityActivityTestRule.launchActivity(new Intent());

        onView(allOf(
                withId(android.support.design.R.id.snackbar_text),
                withText(getString(R.string.error_failed_load_forecast_message))
                )
        ).check(matches(isDisplayed()));

    }

    private String getString(int resId){
        return getInstrumentation().getTargetContext().getString(resId);
    }
}