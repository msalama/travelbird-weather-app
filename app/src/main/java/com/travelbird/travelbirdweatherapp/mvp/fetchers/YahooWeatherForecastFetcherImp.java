package com.travelbird.travelbirdweatherapp.mvp.fetchers;

import android.support.annotation.NonNull;

import com.travelbird.travelbirdweatherapp.data.model.Channel;
import com.travelbird.travelbirdweatherapp.data.model.Forecast;
import com.travelbird.travelbirdweatherapp.data.model.Item;
import com.travelbird.travelbirdweatherapp.data.model.Query;
import com.travelbird.travelbirdweatherapp.data.model.Results;
import com.travelbird.travelbirdweatherapp.data.model.YahooWeatherForecastResponse;
import com.travelbird.travelbirdweatherapp.data.rest.YahooWeatherRestClient;
import com.travelbird.travelbirdweatherapp.mvp.OnFinishedListener;

import java.util.Collections;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class YahooWeatherForecastFetcherImp implements WeatherForecastFetcher {

    public static YahooWeatherForecastFetcherImp newInstance() {
        return new YahooWeatherForecastFetcherImp();
    }


    @Override
    public void listForecastElements(@NonNull final OnFinishedListener<List<Forecast>> listener) {

        YahooWeatherRestClient.
                getInstance().
                getService().
                getWeatherForecast(new Callback<YahooWeatherForecastResponse>() {
                            @Override
                            public void success(
                                    YahooWeatherForecastResponse yahooWeatherForecastResponse,
                                    Response response
                            ) {

                                List<Forecast> daysWeatherForecast =
                                        getWeatherForecastElements(yahooWeatherForecastResponse);

                                listener.onSuccess(daysWeatherForecast);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                listener.onFailure();
                            }
                        });
    }

    private List<Forecast> getWeatherForecastElements(
            YahooWeatherForecastResponse yahooWeatherForecastResponse
    ) {

        Query query = yahooWeatherForecastResponse.getQuery();
        if (query == null) {
            return Collections.emptyList();
        }

        Results results = query.getResults();
        if (results == null) {
            return Collections.emptyList();
        }

        Channel channel = results.getChannel();
        if (channel == null) {
            return Collections.emptyList();
        }

        Item item = channel.getItem();
        if (item == null) {
            return Collections.emptyList();
        }

        List<Forecast> daysForecast = item.getForecast();
        if (daysForecast == null) {
            return Collections.emptyList();
        }

        return item.getForecast();
    }
}
