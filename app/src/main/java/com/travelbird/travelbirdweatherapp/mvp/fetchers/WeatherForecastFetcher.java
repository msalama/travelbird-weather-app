package com.travelbird.travelbirdweatherapp.mvp.fetchers;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.travelbird.travelbirdweatherapp.data.model.Forecast;
import com.travelbird.travelbirdweatherapp.mvp.OnFinishedListener;

import java.util.List;

public interface WeatherForecastFetcher {

    void listForecastElements(@NonNull OnFinishedListener<List<Forecast>> listener);
}
