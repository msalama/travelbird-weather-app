package com.travelbird.travelbirdweatherapp.mvp.presenters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.travelbird.travelbirdweatherapp.data.model.Forecast;
import com.travelbird.travelbirdweatherapp.mvp.OnFinishedListener;
import com.travelbird.travelbirdweatherapp.mvp.fetchers.WeatherForecastFetcher;
import com.travelbird.travelbirdweatherapp.mvp.views.WeatherForecastContract;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class WeatherForecastPresenter implements
        WeatherForecastContract.UserActionsListener {

    private WeatherForecastContract.View view;
    private WeatherForecastFetcher weatherForecastFetcher;

    public WeatherForecastPresenter(
            @NonNull WeatherForecastContract.View view,
            @NonNull WeatherForecastFetcher weatherForecastFetcher
    ) {
        this.view = checkNotNull(view);
        this.weatherForecastFetcher = checkNotNull(weatherForecastFetcher);
    }

    @Override
    public void loadWeatherForecastElements() {

        getView().showProgress();

        weatherForecastFetcher.listForecastElements(new OnFinishedListener<List<Forecast>>() {
            @Override
            public void onSuccess(@Nullable List<Forecast> daysWeatherForecast) {

                if (getView() == null) {
                    return;
                }

                getView().hideProgress();

                getView().showWeatherForecastElements(daysWeatherForecast);

            }

            @Override
            public void onFailure() {

                if (getView() == null) {
                    return;
                }

                getView().hideProgress();

                getView().showFailureMessage();
            }
        });
    }

    @Override
    public void stopPresenter() {
        this.view = null;
    }

    private WeatherForecastContract.View getView() {
        return view;
    }
}
