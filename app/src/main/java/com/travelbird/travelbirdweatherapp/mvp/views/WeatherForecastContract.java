package com.travelbird.travelbirdweatherapp.mvp.views;

import android.support.annotation.NonNull;

import com.travelbird.travelbirdweatherapp.data.model.Forecast;

import java.util.List;


public interface WeatherForecastContract {

    interface View {

        void showProgress();

        void hideProgress();

        void showWeatherForecastElements(List<Forecast> weatherForecastElements);

        void showFailureMessage();

    }

    interface UserActionsListener {

        void loadWeatherForecastElements();

        void stopPresenter();
    }
}
