package com.travelbird.travelbirdweatherapp.util;

public final class Constants {

    private Constants() {
        // no instance
    }

    public static final String FORECAST_IMAGE_BASE_URL = "http://l.yimg.com/a/i/us/we/52/";

    //Api UrL
    public static final String BACKEND_API_URL = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22amsterdam%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
}
