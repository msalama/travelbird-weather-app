package com.travelbird.travelbirdweatherapp.data.rest;

import com.travelbird.travelbirdweatherapp.data.api.YahooWeatherApi;
import com.travelbird.travelbirdweatherapp.util.Constants;

import retrofit.RestAdapter;

public class YahooWeatherRestClient {

    private static YahooWeatherRestClient instance;

    private YahooWeatherApi yahooWeatherApi;

    public static YahooWeatherRestClient getInstance() {
        if (instance == null) {
            instance = new YahooWeatherRestClient();
        }
        return instance;
    }

    public YahooWeatherRestClient() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.BACKEND_API_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        yahooWeatherApi = restAdapter.create(YahooWeatherApi.class);
    }

    public YahooWeatherApi getService() {
        return yahooWeatherApi;
    }

}
