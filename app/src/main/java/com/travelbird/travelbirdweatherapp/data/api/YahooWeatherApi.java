package com.travelbird.travelbirdweatherapp.data.api;

import com.travelbird.travelbirdweatherapp.data.model.YahooWeatherForecastResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface YahooWeatherApi {

    @GET("/")
    void getWeatherForecast(Callback<YahooWeatherForecastResponse> callback);
}
