package com.travelbird.travelbirdweatherapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.travelbird.travelbirdweatherapp.R;
import com.travelbird.travelbirdweatherapp.data.model.Forecast;
import com.travelbird.travelbirdweatherapp.util.Constants;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class WeatherForecastElementsAdapter
        extends RecyclerView.Adapter<WeatherForecastElementsAdapter.ForecastElementViewHolder> {

    private static final String TAG = "DaysWeatherForecastAdapter";

    private List<Forecast> forecastElementsList;
    private Context context;
    private ForecastItemListener forecastItemListener;

    public WeatherForecastElementsAdapter(
            Context context,
            List<Forecast> forecastElementsList,
            ForecastItemListener forecastItemListener
    ) {
        this.forecastItemListener = forecastItemListener;
        this.context = context;
        setList(forecastElementsList);
    }

    @Override
    public ForecastElementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View forecastView = inflater.inflate(R.layout.forecast_item, parent, false);
        ForecastElementViewHolder holder =
                new ForecastElementViewHolder(
                        forecastView,
                        forecastItemListener
                );

        return holder;
    }

    @Override
    public void onBindViewHolder(
            ForecastElementViewHolder forecastElementViewHolder,
            int position
    ) {

        Forecast forecastElement = forecastElementsList.get(position);

        forecastElementViewHolder.forecastLowestTemperature.setText(forecastElement.getLow());

        forecastElementViewHolder.forecastHighestTemperature.setText(forecastElement.getHigh());

        forecastElementViewHolder.forecastDate.setText(forecastElement.getDate());

        forecastElementViewHolder.forecastDay.setText(forecastElement.getDay());

        String forecastImageUrl =
                Constants.FORECAST_IMAGE_BASE_URL.concat(
                        forecastElement.getCode()).concat(".gif");

        Picasso.with(context).load(forecastImageUrl)
                .placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher)
                .into(forecastElementViewHolder.forecastImage);

    }

    public Forecast getItem(int position) {
        return forecastElementsList.get(position);
    }

    public void replaceData(List<Forecast> forecastElementsList) {
        setList(forecastElementsList);
        notifyDataSetChanged();
    }

    private void setList(@NonNull List<Forecast> forecastElementsList) {
        this.forecastElementsList = checkNotNull(forecastElementsList);
    }

    @Override
    public int getItemCount() {
        return forecastElementsList.size();
    }

    public class ForecastElementViewHolder
            extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView forecastImage;
        public TextView  forecastLowestTemperature;
        public TextView  forecastHighestTemperature;
        public TextView  forecastDate;
        public TextView  forecastDay;

        private ForecastItemListener forecastItemListener;

        public ForecastElementViewHolder(
                View itemView,
                ForecastItemListener forecastItemListener
        ) {
            super(itemView);

            this.forecastItemListener = forecastItemListener;

            forecastImage = (ImageView) itemView.findViewById(R.id.forecast_day_image);

            forecastLowestTemperature =
                    (TextView) itemView.findViewById(R.id.forecast_day_lowest_temperature);

            forecastHighestTemperature =
                    (TextView) itemView.findViewById(R.id.forecast_day_highest_temperature);

            forecastDate = (TextView) itemView.findViewById(R.id.forecast_date);

            forecastDay = (TextView) itemView.findViewById(R.id.forecast_day);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick: ");
            int position = getAdapterPosition();
            Forecast device = getItem(position);
            forecastItemListener.onForecastElementClick(device);
        }

    }

    public interface ForecastItemListener {

        void onForecastElementClick(Forecast forecastElement);
    }
}
