package com.travelbird.travelbirdweatherapp.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.travelbird.travelbirdweatherapp.R;

public class WeatherForecastListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast_list);
    }
}
