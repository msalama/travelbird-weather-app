package com.travelbird.travelbirdweatherapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.travelbird.travelbirdweatherapp.Injector;
import com.travelbird.travelbirdweatherapp.R;
import com.travelbird.travelbirdweatherapp.adapters.WeatherForecastElementsAdapter;
import com.travelbird.travelbirdweatherapp.data.model.Forecast;
import com.travelbird.travelbirdweatherapp.mvp.fetchers.YahooWeatherForecastFetcherImp;
import com.travelbird.travelbirdweatherapp.mvp.presenters.WeatherForecastPresenter;
import com.travelbird.travelbirdweatherapp.mvp.views.WeatherForecastContract;

import java.util.Collections;
import java.util.List;

public class WeatherForecastListFragment
        extends Fragment implements WeatherForecastContract.View {

    private ProgressBar progressBarLoading;
    private WeatherForecastPresenter weatherForecastPresenter;
    private WeatherForecastElementsAdapter weatherForecastElementsAdapter;

    WeatherForecastElementsAdapter.ForecastItemListener forecastItemListener =
            new WeatherForecastElementsAdapter.ForecastItemListener() {

                @Override
                public void onForecastElementClick(Forecast forecastElement) {

                }
            };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        weatherForecastPresenter = new WeatherForecastPresenter(
                this,
                Injector.provideYahooWeatherForecastFetcher()
        );

        weatherForecastElementsAdapter = new WeatherForecastElementsAdapter(
                getContext(),
                Collections.<Forecast>emptyList(),
                forecastItemListener
        );
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_forecast_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //init recycle view

        RecyclerView forecastList =
                (RecyclerView) view.findViewById(R.id.forecast_list);

        int numColumns = getContext().getResources().getInteger(R.integer.num_columns);

        forecastList.setHasFixedSize(true);
        forecastList.setLayoutManager(new GridLayoutManager(getContext(), numColumns));

        forecastList.setAdapter(weatherForecastElementsAdapter);

        progressBarLoading = (ProgressBar) view.findViewById(R.id.loading_progressBar);

    }

    @Override
    public void onStart() {
        super.onStart();
        weatherForecastPresenter.loadWeatherForecastElements();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        weatherForecastPresenter.stopPresenter();
    }

    @Override
    public void showProgress() {
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarLoading.setVisibility(View.GONE);
    }

    @Override
    public void showWeatherForecastElements(List<Forecast> forecastElements) {
        weatherForecastElementsAdapter.replaceData(forecastElements);
    }

    @Override
    public void showFailureMessage() {
        Snackbar.make(
                getView(),
                getString(R.string.error_failed_load_forecast_message),
                Snackbar.LENGTH_SHORT
        ).show();
    }
}
