package com.travelbird.travelbirdweatherapp.mvp.presenters;

import com.google.common.collect.Lists;
import com.travelbird.travelbirdweatherapp.data.model.Forecast;
import com.travelbird.travelbirdweatherapp.mvp.OnFinishedListener;
import com.travelbird.travelbirdweatherapp.mvp.fetchers.WeatherForecastFetcher;
import com.travelbird.travelbirdweatherapp.mvp.views.WeatherForecastContract;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import java.util.List;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class WeatherForecastPresenterTest {

    private static List<Forecast> forecastList =
            Lists.newArrayList(new Forecast());

    @Mock
    private WeatherForecastFetcher weatherForecastFetcher;

    @Mock
    private WeatherForecastContract.View weatherForecastView;

    @Captor
    private ArgumentCaptor<OnFinishedListener<List<Forecast>>> listForecastElementsCallbackCaptor;

    private WeatherForecastPresenter weatherForecastPresenter;

    @Before
    public void setupWeatherForecastPresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        weatherForecastPresenter = new WeatherForecastPresenter(
                weatherForecastView,
                weatherForecastFetcher
        );

        weatherForecastPresenter.loadWeatherForecastElements();

    }

    @Test
    public void loadDevicesFromBackendAndLoadIntoView() {

        verify(weatherForecastView).showProgress();

        verify(weatherForecastFetcher).listForecastElements(
                listForecastElementsCallbackCaptor.capture()
        );

        listForecastElementsCallbackCaptor.getValue().onSuccess(forecastList);

        verify(weatherForecastView).hideProgress();
        verify(weatherForecastView).showWeatherForecastElements(forecastList);
    }

    @Test
    public void showFailureMessageWhenBackendReturnError() {

        verify(weatherForecastView).showProgress();

        verify(weatherForecastFetcher).listForecastElements(
                listForecastElementsCallbackCaptor.capture()
        );

        listForecastElementsCallbackCaptor.getValue().onFailure();

        verify(weatherForecastView).hideProgress();
        verify(weatherForecastView).showFailureMessage();
    }

    @Test
    public void testStopPresenterWhenSuccessReturnFromBackend() {

        weatherForecastPresenter.stopPresenter();

        verify(weatherForecastView).showProgress();

        verify(weatherForecastFetcher).listForecastElements(
                listForecastElementsCallbackCaptor.capture()
        );

        listForecastElementsCallbackCaptor.getValue().onSuccess(forecastList);

        verify(weatherForecastView, never()).hideProgress();
        verify(weatherForecastView, never()).showWeatherForecastElements(forecastList);
    }

    @Test
    public void testStopPresenterWhenFailureReturnFromBackend() {

        weatherForecastPresenter.stopPresenter();

        verify(weatherForecastView).showProgress();

        verify(weatherForecastFetcher).listForecastElements(
                listForecastElementsCallbackCaptor.capture()
        );

        listForecastElementsCallbackCaptor.getValue().onFailure();

        verify(weatherForecastView, never()).hideProgress();
        verify(weatherForecastView, never()).showFailureMessage();
    }

}