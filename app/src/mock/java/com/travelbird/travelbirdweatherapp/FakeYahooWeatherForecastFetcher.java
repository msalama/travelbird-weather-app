package com.travelbird.travelbirdweatherapp;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.travelbird.travelbirdweatherapp.data.model.Forecast;
import com.travelbird.travelbirdweatherapp.mvp.OnFinishedListener;
import com.travelbird.travelbirdweatherapp.mvp.fetchers.WeatherForecastFetcher;

import java.util.List;

public class FakeYahooWeatherForecastFetcher implements WeatherForecastFetcher {

    private static boolean IS_SUCCESS;
    private static List<Forecast> FORECAST_LIST;

    public static FakeYahooWeatherForecastFetcher newInstance() {
        return new FakeYahooWeatherForecastFetcher();
    }

    @Override
    public void listForecastElements(@NonNull OnFinishedListener<List<Forecast>> listener) {
        if (IS_SUCCESS) {
            listener.onSuccess(FORECAST_LIST);
        } else {
            listener.onFailure();
        }
    }

    @VisibleForTesting
    public static void setWeatherForecastList(List<Forecast> forecastList) {
        FORECAST_LIST = forecastList;
    }

    @VisibleForTesting
    public static void setIsSuccess(boolean isSuccess) {
        IS_SUCCESS = isSuccess;
    }
}
