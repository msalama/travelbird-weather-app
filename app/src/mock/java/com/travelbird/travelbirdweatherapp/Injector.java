package com.travelbird.travelbirdweatherapp;

import com.travelbird.travelbirdweatherapp.mvp.fetchers.WeatherForecastFetcher;
import com.travelbird.travelbirdweatherapp.mvp.fetchers.YahooWeatherForecastFetcherImp;

public class Injector {

    private Injector() {
        // no instance
    }

    public synchronized static WeatherForecastFetcher provideYahooWeatherForecastFetcher() {
        return FakeYahooWeatherForecastFetcher.newInstance();
    }

}
